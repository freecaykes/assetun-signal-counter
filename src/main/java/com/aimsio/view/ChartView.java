package com.aimsio.view;

import com.aimsio.backend.Connector;
import com.aimsio.backend.SignalService;
import com.vaadin.addon.charts.Chart;
import com.vaadin.addon.charts.model.*;
import com.vaadin.addon.charts.model.style.SolidColor;
import com.vaadin.server.Page;
import com.vaadin.shared.ui.ContentMode;
import com.vaadin.ui.*;
import com.vaadin.ui.Label;

import java.sql.Connection;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.ThreadLocalRandom;

public class ChartView extends VerticalLayout {

    // Components
    private Chart timeline;
    private VerticalLayout titleBar;
    private Label title;
    private final String titleString = "AssetUN Received Signals over Time";

    private static final String ACTIVE = "Active";
    private static final String ENGAGED = "Engaged";
    private static final String OVERRIDE = "Override";
    private static final String ALL = "All";

    private DataSeries displayed;

    private String currentStatus = ALL;
    private String currentAssetUN = ALL;

    private boolean doFilter = false;

    public enum FilterToggled{
        STATUS, ASSETUN
    }

    public ChartView() {
        Page.getCurrent().setTitle("AssetUN Signals");
        Page.getCurrent().addBrowserWindowResizeListener(event -> resizeLabel(event.getWidth()));

        titleBar = new VerticalLayout();
        title = new Label();
        resizeLabel(Page.getCurrent().getBrowserWindowWidth());

        this.addComponent(titleBar);

        // init Chart UI
        timeline = new Chart(ChartType.LINE);
        timeline.setSizeFull();
        timeline.setTimeline(true);

        Configuration configuration = timeline.getConfiguration();
        configuration.getChart().setZoomType(ZoomType.XY);

//        configuration.getTitle().setText("Number of Signals over Time");
        configuration.getxAxis().setType(AxisType.DATETIME);
        configuration.getyAxis().setMin(0);

        configuration.getyAxis().setTitle("# of Signals");

        configuration.getxAxis().getLabels().setEnabled(false);
        configuration.getLegend().setEnabled(true);

        // init Data
        displayed = new DataSeries("All");
        SignalService.populateDataSeries(displayed, "all", null);

        configuration.setSeries(displayed);

        this.addComponent(timeline);

        // Bottom Filter ComboBox pane
        HorizontalLayout filterPane = new HorizontalLayout();
        filterPane.setSizeFull();
        filterPane.setSpacing(false);

        List<String> unList = SignalService.getAllDistinctAssetUN();
        java.util.Collections.sort(unList);
        unList.add(0, ALL);

        ComboBox<String> assetUNComboBox = new ComboBox<String>("AssetUN:",  unList);
        assetUNComboBox.setEmptySelectionAllowed(false);
        assetUNComboBox.setValue(ALL);
        assetUNComboBox.addValueChangeListener( event -> comboBoxEvent(event.getValue(), FilterToggled.ASSETUN) );
        filterPane.addComponent(assetUNComboBox);

        ComboBox<String> status = new ComboBox<String>("Status:", new HashSet<>(Arrays.asList(ALL, ACTIVE, ENGAGED, OVERRIDE)));
        status.setEmptySelectionAllowed(false);
        status.setValue(ALL);
        status.addValueChangeListener( event -> comboBoxEvent(event.getValue(), FilterToggled.STATUS));
        filterPane.addComponent(status);
        this.addComponent(filterPane);

        Button filterTrigger = new Button("Filter");
        filterTrigger.addClickListener( event -> filter());
        this.addComponent(filterTrigger);
    }

    @Deprecated // for Debug
    private static void populate(DataSeries series){
        Date now = new Date();
        Calendar cal = Calendar.getInstance();
        cal.setTime(now);
        for (int i = 0; i<365; i++){
            addData(series,  cal.getTime(),  ThreadLocalRandom.current().nextInt(0, 500 + 1) );
            cal.add( Calendar.DATE, 1 );
        }
    }

    @Deprecated // for Debug
    private static void addData(DataSeries series, Date date, int signalValue){
        series.add( new DataSeriesItem(date.toInstant(), signalValue) );
    }

    // EVENTS

    /**
     * ComboBox event
     *
     * Set current selected Filters based on status
     * @param value
     * @param toggled
     */
    private void comboBoxEvent(String value, FilterToggled toggled){
        switch (toggled){
            case STATUS:
                if (!currentStatus.equals(value)) {
                    currentStatus = value;
                    doFilter = true;
                }
                break;
            case ASSETUN:
                if (!currentAssetUN.equals(value)) {
                    currentAssetUN = value;
                    doFilter = true;
                }
                break;
        }
    }

    /**
     * Button Event
     *
     * Re-populate displayed DataSeries from DB per filter based on ComboBox choices
     */
    private void filter(){
        if(doFilter) { // do nothing when comboBox has not changed
            displayed.clear();
            doFilter = false;
            SignalService.populateDataSeries(displayed, currentStatus.toLowerCase(), currentAssetUN.toLowerCase());
            displayed.setName("AssetUN: " + currentAssetUN + " Status: " + currentStatus);
            timeline.getConfiguration().setSeries(displayed);
            timeline.drawChart();
        }
    }

    /**
     * WindowSize Changed
     *
     * @param size - Browser Window size
     */
    private void resizeLabel(int size){
        titleBar.removeAllComponents();
        if(size < 400){
            title.setValue("<h5>" + titleString + "</h5>");
        }else if(size >= 400 && size < 500){
            title.setValue("<h4>" + titleString + "</h4>");
        }else if(size >= 500 && size < 600){
            title.setValue("<h3>" + titleString + "</h3>");
        }else if(size >= 600 && size < 700){
            title.setValue("<h2>" + titleString + "</h2>");
        }else{
            title.setValue("<h1>" + titleString + "</h1>");
        }

        title.setContentMode(ContentMode.HTML);
        titleBar.addComponent(title);
    }



}
