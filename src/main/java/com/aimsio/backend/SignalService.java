package com.aimsio.backend;

import com.vaadin.addon.charts.model.DataSeries;
import com.vaadin.addon.charts.model.DataSeriesItem;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;


import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class SignalService {

    private static final String TABLE_NAME = Connector.getDATABASE() + ".signal";

    private static Connection connection;

    private static  DateFormat inputFormatter = new SimpleDateFormat("yyyy-MM-dd");
    /**
     * Initiate connection to AWS Database
     *
     * @throws SQLException
     */
    public static void initConnection() throws SQLException {
        if(connection == null || connection.isClosed() ){
            connection = Connector.connectJDBC();
        }
    }

    public static void closeConnection() throws SQLException {
        if(connection != null && !connection.isClosed()) {
            connection.close();
        }
    }

    /**
     * Retrieve ResultSet from query on open connection
     * @param query - query to run
     * @return - ResultSet on query
     */
    private static ResultSet getResultSetFromQuery(String query){
        try {
            initConnection();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        String threadID = UUID.randomUUID().toString();
        QueryRunnable queryRunnable = new QueryRunnable(connection, query);
        Thread queryThread = new Thread( queryRunnable, threadID);

        try {
            queryThread.start();
            queryThread.join();

            ResultSet rs = queryRunnable.getResultSet();
            return rs;
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        return null;
    }

    /**
     * unused
     * @return - All distinct different dates
     */
    public static List<Date> getAllDistinctDates(){
        String query = "SELECT DISTINCT entry_date FROM " + TABLE_NAME + ";";
        List<Date> entry_dates = new LinkedList<>();

        ResultSet rs = getResultSetFromQuery(query);

        try {
            while (rs.next()) {
                entry_dates.add( rs.getDate(1) );
            }
        } catch (SQLException e){
            e.printStackTrace();
        }

        return entry_dates;
    }

    /**
     * 
     * @return - All Distinct AssetUN
     */
    public static List<String> getAllDistinctAssetUN(){
        String query = "SELECT DISTINCT assetun FROM " + TABLE_NAME + ";";
        List<String> assetun = new LinkedList<>();

        ResultSet rs = getResultSetFromQuery(query);

        try {
            while (rs.next()) {
                assetun.add( rs.getString(1) );
            }
        } catch (SQLException e){
            e.printStackTrace();
        }

        return assetun;
    }

    /**
     * Populate Dataseries time vs count on based on status assetun
     * @param series - DataSeries to fill
     * @param status - Status to filter by
     * @param assetun - AssetUN to filter by
     */
    public static void populateDataSeries(DataSeries series, String status, String assetun){
        String query = "select entry_date, count(*) from " + TABLE_NAME;

        // append for where if status or assetun exists
        String condition = " where ";
        int originalLen = condition.length();
        String statusCond = (status != null && !status.equals("all") && !status.equals("")) ? " status=\'" + status + "\'" : "";
        String assetUNCond = (assetun != null && !assetun.equals("all") && !assetun.equals("")) ? " assetun=\'" + assetun + "\'" : "";

        if(statusCond.length() > 0 && assetUNCond.length() > 0){
            condition += statusCond + " and " + assetUNCond;
        }else if(statusCond.length() > 0 && assetUNCond.length() == 0){
            condition += statusCond;
        }else if(assetUNCond.length() > 0 && statusCond.length() == 0){
            condition += assetUNCond;
        }

        query += (condition.length() > originalLen) ? condition : "";
        query += " group by entry_date;";

        ResultSet rs = getResultSetFromQuery(query);

        try {
            while (rs.next()) {
                Date rowDate = inputFormatter.parse(rs.getDate(1).toString());
//                System.out.println( rs.getDate(1).toString() + rs.getInt(2));
                series.add( new DataSeriesItem(rowDate.toInstant() , rs.getInt(2)));
            }
        } catch (SQLException e){
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }

    }




}
