package com.aimsio.backend;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class QueryRunnable implements Runnable{
/**
 * Runnable class for using Connection to fetch
 * the ResultSet based on passed on the passed Query
 **/

    String query;
    Connection connection;
    ResultSet resultSet;

    /**
     * Initiate with connection and query - passed to seperate Thread
     *
     * @param connection
     * @param query
     */
    public QueryRunnable(Connection connection, String query){
        this.connection = connection;
        this.query = query;
    }

    /**
     * Run Query on Thread.start()
     */
    public void run(){
        Statement statement = null;
        try {

            System.out.println("Creating statement: " + query);
            statement = (Statement) connection.createStatement();
            resultSet = statement.executeQuery(query);

        } catch (SQLException e) {
            //Handle errors for JDBC
            e.printStackTrace();
        } catch (Exception e) {
            //Handle errors for Class.forName
            e.printStackTrace();
        }
    }

public ResultSet getResultSet() {
        return resultSet;
    }
}