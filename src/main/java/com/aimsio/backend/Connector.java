package com.aimsio.backend;


import java.io.Serializable;
import java.sql.*;

public class Connector implements Serializable {
    /**
     * Setup JBDC Connection based on credentials
     *
     */

    private static final String MySQL_CLASSNAME = "com.mysql.jdbc.Driver";
    private static final String ORACLE_CASSNAME = "oracle.jdbc.driver.OracleDriver";
    private static final String HOST = "aimsio-assignment.cutnmbp0viai.us-west-2.rds.amazonaws.com";
    private static final String PORT = "3306";

    public static String getDATABASE() {
        return DATABASE;
    }

    private static final String DATABASE = "assets";
    private static final String REMOTE_DATABASE_USERNAME = "user0326";
    private static final String DATABASE_USER_PASSWORD = "9g5G4RK";

    public static Connection connectJDBC() {
        System.out.println("---- MySQL JDBC Connection -------");

        try {
            Class.forName(MySQL_CLASSNAME);
        } catch (ClassNotFoundException e) {
            System.out.println("Missing MySQL JDBC Driver");
            e.printStackTrace();
            return null;
        }

        System.out.println(MySQL_CLASSNAME + " Registered");
        Connection connection = null;

        try {
            connection = DriverManager. getConnection("jdbc:mysql://" + HOST  + ":" + PORT + "/" + DATABASE, REMOTE_DATABASE_USERNAME, DATABASE_USER_PASSWORD);
        } catch (SQLException e) {
            System.out.println("Connection Failed:\n" + e.getMessage());
        }

        if (connection != null) {
            System.out.println("SUCCESS");
            return connection;
        } else {
            System.out.println("FAILURE");
            return null;
        }
    }

}
